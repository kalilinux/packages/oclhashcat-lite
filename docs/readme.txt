oclHashcat-lite v0.15
=====================

NV users require ForceWare 310.32 or later
AMD users require Catalyst 13.1 -exact-

##
## Features
##

- Worlds fastest LM, NTLM, MD5, SHA1, SHA256 and DES cracker
- Free
- Multi-GPU (up to 128 gpus)
- Multi-OS (Linux & Windows native binaries)
- Multi-Platform (OpenCL & CUDA support)
- Multi-Algo (see below)
- Low resource utilization, you can still watch movies or play games while cracking
- Focuses one-shot, lightweight hashes
- Supports mixed GPU types
- Supports markov attack
- Supports mask attack
- Supports distributed cracking
- Supports pause / resume while cracking
- Supports sessions
- Supports restore
- Supports hex-salt
- Supports hex-charset
- Integrated thermal watchdog

##
## Modes
##

- MD5
- md5($pass.$salt)
- Joomla
- SHA1
- nsldap, SHA-1(Base64), Netscape LDAP SHA
- sha1($pass.$salt)
- nsldaps, SSHA-1(Base64), Netscape LDAP SSHA
- Oracle 11g, SHA-1(Oracle)
- MSSQL(2000)
- MSSQL(2005)
- MySQL
- MD4
- md4($pass.$salt)
- NTLM
- Domain Cached Credentials, mscash
- SHA256
- sha256($pass.$salt)
- descrypt, DES(Unix), Traditional DES
- SHA512
- sha512($pass.$salt)
- Cisco-PIX MD5
- Double MD5
- vBulletin < v3.8.5
- vBulletin > v3.8.5
- IPB2+, MyBB1.2+
- LM
- Oracle 7-10g, DES(Oracle)
- SHA-3(Keccak)

##
## Tested OS's
##

- All Windows and Linux versions should work on both 32 and 64 bit

##
## Tested GPU's
##

- All CUDA and Stream enabled cards should work

To get started run the example scripts or check out docs/examples.txt

--
atom
